

#
#
#	0. update the domain in certify: certify.sh
#	1. increase the "goodest" version in the _Logistics/habitat/pyproject.toml
#	2. send:
#		[Origin OS] python3 /habitat/_Logistics/logistics/1.send.py
#


'''
	[ ] Increase the version of the frontend assets.
	
		# vite.config.js
		# 
'''

'''
	rm -rf /homesteads/veganSink.com/calculators/reception/factory/backend/frontend_dist
'''

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'modules_pip'
])


#\
#
from os.path import dirname, join, normpath
import pathlib
import sys
import os
#
#
import paramiko
#
#
import vessels.SSH.send as SSH_send
#
#
from variables import variables
#
#/

the_private_key_path = variables ["private key"]

the_distant_openers_directory = variables ["to"] ["openers"]
#the_distant_openers_directory = os.path.dirname (the_distant_openers_path)

'''
	SSH_play ({
		"connection": {
			"host":
			"port":
			"user": 
			"private_key_path": ""
		},		
		
		"play": "ls"
	})
'''
def SSH_play (packet):
	host = packet ["connection"] ["host"]
	port = packet ["connection"] ["port"]
	username = packet ["connection"] ["user"]
	private_key_path = packet ["connection"] ["private_key_path"]

	play = packet ["play"]


	# Initialize SSH client
	client = paramiko.SSHClient ()
	client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

	# Load private key
	private_key = paramiko.RSAKey.from_private_key_file (private_key_path)

	# Connect to SSH server
	client.connect(hostname=host, port=port, username=username, pkey=private_key)

	# Execute SSH command
	stdin, stdout, stderr = client.exec_command (play)

	# Print output of the command
	for line in stdout:
		print(line.strip())

	# Close SSH connection
	client.close()
	
	return;

def make_directory ():
	host = variables ["to"] ["addresses"] [0] ["address"]
	port = 22
	username = 'root'

	SSH_play ({
		"connection": {
			"host": variables ["to"] ["addresses"] [0] ["address"],
			"port": 22,
			"user": 'root',
			"private_key_path": the_private_key_path
		},		
		
		"play": f"mkdir '{ the_distant_openers_directory }'"
	})


def send_openers ():
	SSH_send.splendidly ({
		"private key": variables ["private key"],
		
		"from": {
			"directory": variables ["from"] ["openers"]
		},
		
		"to": {
			"address": variables ["to"] ["addresses"] [0] ["address"],
			"directory": variables ["to"] ["openers"]
		}
	})

def send_habitat ():
	SSH_send.splendidly ({
		"private key": variables ["private key"],
		
		"from": {
			"directory": variables ["from"] ["habitat"]
		},
		
		"to": {
			"address": variables ["to"] ["addresses"] [0] ["address"],
			"directory": variables ["to"] ["habitat"]
		}
	})

#make_directory ()
send_openers ()
send_habitat ()

