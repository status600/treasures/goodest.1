




#/
#
#	clear the environment
#
#
deactivate
(cd /habitat_physical && rm -rf .venv)
(cd /habitat_physical && rm requirements.txt)
#
#\




#/
#
#	build UV environment
#
sleep 1

echo ""
echo "compiling"
echo "----"
(cd /habitat && uv pip compile pyproject.toml -o /habitat_physical/requirements.txt)
echo "----"

sleep 2

(cd /habitat_physical && uv venv)
#
#\



. /habitat_physical/.venv/bin/activate
(cd /habitat_physical && uv pip sync requirements.txt)


cd /habitat_physical 


(cp /habitat/goodest_essence.py /habitat_physical/goodest_essence.py)