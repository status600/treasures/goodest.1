




#/
#
#	obtain the modules
#
#
apt install python3-pip -y
apt install haproxy -y
#
#\

#/
#
#	pip kits
#
#	https://stackoverflow.com/questions/75608323/how-do-i-solve-error-externally-managed-environment-every-time-i-use-pip-3
#
rm /usr/lib/python3.12/EXTERNALLY-MANAGED
pip install uv
#
#\

#/
#
#	mongo
#
apt-get install gnupg curl -y
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
   gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-7.0.list
apt-get update -y
apt-get install -y mongodb-org
#
#\


mkdir /habitat_physical
