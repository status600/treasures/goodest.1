


def Calcium ():
	return {
		"labels": [
			"Calcium"
		],
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "13/10"
						}
					}
				}
			}
		},
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}

