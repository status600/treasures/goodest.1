

''''
https://nap.nationalacademies.org/catalog/10490/dietary-reference-intakes-for-energy-carbohydrate-fiber-fat-fatty-acids-cholesterol-protein-and-amino-acids
"'''

'''
	200 pounds = 90000 grams
		
		28 / 90000 
			= 0.00031 
			= 0.031%
'''

def Dietary_Fiber ():
	return {
		"labels": [
			"Dietary Fiber"
		],
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "28"
						}
					}
				}
			},
		},
		"notes": [
		
		],
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels",
			"https://www.mayoclinic.org/healthy-lifestyle/nutrition-and-healthy-eating/in-depth/fiber/art-20043983"
		]
	}