


def Calories ():
	return {
		"labels": [
			"Calories"
		],
		"criteria": {
			"RDA": {
				"energy": {
					"per Earth day": {
						"Food Calories": {
							"fraction string": "2000"
						}
					}
				}
			}
		}
	}