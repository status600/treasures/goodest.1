

'''

'''
def find (
	DSLD_ID
):
	return {
		"name": "NIH",
		"link": f"https://dsld.od.nih.gov/label/{ DSLD_ID }",
		"API info": "https://dsld.od.nih.gov/api-guide",
		"citation": "National Institutes of Health, Office of Dietary Supplements. Dietary Supplement Label Database, 2023. https://dsld.od.nih.gov/. "
	}