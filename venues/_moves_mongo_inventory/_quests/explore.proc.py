

#
# 	Caution: 
#		* hasn't worked
#		* [0] status checks	
#

#
#	This refreshes the natures of foods
#
#

# python3 /habitat/venues/_moves_mongo_inventory/_quests/explore.proc.py

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])

import rich

from goodest.adventures.monetary.quests.search_goods import search_goods
search_proceeds = search_goods ({
	"filters": {
		"string": "rice",
		"include": {
			"food": True,
			"supp": False,
			"meals": True
		},
		"limit": 10
	}
})

rich.print_json (data = search_proceeds)

