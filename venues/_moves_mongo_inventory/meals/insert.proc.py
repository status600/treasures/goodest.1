
#
# 	Caution: 
#		This has worked once,
#		but there aren't any status checks for this.	
#

#
#	This is for inserting meals.
#
#

#
#
# 	python3 /habitat/venues/_moves_mongo_inventory/meals/insert.proc.py
#
#

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])


from goodest.adventures.monetary.DB.goodest_inventory.collect_meals.document.insert import insert_meal
insert_meal ({
	"name": "rice and beans",
	"formulate": {
		"IDs_with_amounts": [
			{
				"FDC_ID": "2471166",
				"grams": 1000
			},
			{
				"FDC_ID": "2425001",
				"grams": 2000
			}
		]
	}
})