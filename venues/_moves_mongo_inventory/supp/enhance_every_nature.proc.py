
#
# 	Caution: 
#		This has worked once,
#		but there aren't any status checks for this.	
#

#
#	TODO:
#		enhance from: [ "saved data", "source" ]
#

# 	python3 /habitat/venues/_moves_mongo_inventory/supp/enhance_every_nature.proc.py

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])


#\
#
import time
#
#
import rich
#
#
import ships.modules.exceptions.parse as parse_exception
from ships.flow.simultaneous_v2 import simultaneously_v2
#
#
from goodest.adventures.monetary.DB.goodest_inventory._treasures.documents.every import find_every_treasure
from goodest.adventures.monetary.DB.goodest_inventory.supps.document.enhance_nature import enhance_supp_nature
#
#/


#
#
#	TODO:
#		maybe.. loop 5 times until finished if from "source"
#
#		This should really use "emblem" since ID might not be limited by index to 1.
#

#\
#
#	Options
#		origin: "saved" or "source"
#		every: whether to enhance every food, or a list of foods
#
#
origin = "saved"
every = "no"
#
#/

'''
{ "nature.identity.DSLD ID": "276336" }
'''


if (every == "yes"):
	[ ID_Scroll, Skipped ] = find_every_treasure ({
		"collection": "supp",
		"path": "nature.identity.DSLD ID"
	})
else:
	Skipped = 0
	
	#
	#	ID_Scroll = [ "270619", "180409" ]
	#
	ID_Scroll = [ '276336' ]
	



problems = []
problem_IDs = []
finished = []

def move (item):
	DSLD_ID = item;

	try:
		the_enhance = enhance_supp_nature ({
			"origin": origin,
			"DSLD_ID": DSLD_ID,
			"filter": {
				"nature.identity.DSLD ID": DSLD_ID
			}
		})
		
		print ("the_enhance:", the_enhance)
		
		result = f"Processed item: {item}"
		
		finished.append (DSLD_ID)
		
	except Exception as E:
		problem = {
			"exception": parse_exception.now (E).split ("\n"),
			"DSLD_ID": DSLD_ID
		}
		
		problem_IDs.append (DSLD_ID)
		
		print ("problem:", problem)
	
		problems.append (problem)
		
	#return result


proceeds = simultaneously_v2 (
	items = ID_Scroll,
	capacity = 4,
	move = move
)



rich.print_json (data = {
	"problems": problems,
	"problem_IDs": problem_IDs,
	"finished": finished
})

print ("ID_Scroll:", ID_Scroll)
print ("ID_Scroll size:", len (ID_Scroll))
print ("Skipped size:", len (Skipped))
