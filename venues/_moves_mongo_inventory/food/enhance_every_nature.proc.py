
#
# 	Caution: 
#		This has worked,
#		but there aren't any status checks for this.	
#

#
#	This refreshes the natures of foods
#
#		VPN might lead to "forbidden" :(
#

#
#
# 	python3 /habitat/venues/_moves_mongo_inventory/food/enhance_every_nature.proc.py
#
#

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])


from goodest.adventures.monetary.DB.goodest_inventory.foods.document.enhance_nature import enhance_food_nature
from goodest.adventures.monetary.DB.goodest_inventory._treasures.documents.every import find_every_treasure

from ships.flow.simultaneous_v2 import simultaneously_v2
import time
import rich
import ships.modules.exceptions.parse as parse_exception

#\
#
#	Options
#		origin: "saved" or "source"
#		every: whether to enhance every food, or a list of foods
#
#
origin = "saved"
every = "yes"
#
#/



if (every == "yes"):
	[ ID_Scroll, Skipped ] = find_every_treasure ({
		"collection": "food",
		"path": "nature.identity.FDC ID"
	})
else:
	Skipped = []
	ID_Scroll = []




problems = []
problem_IDs = []
finished = []


'''
{ "nature.identity.FDC ID": "2655306" }
'''
def move (item):
	try:
		FDC_ID = item;
		enhance_food_nature ({
			
			#
			#
			#	saved or source
			#
			#
			"origin": origin,
			
			"FDC_ID": FDC_ID,
			"filter": {
				"nature.identity.FDC ID": FDC_ID
			}
		})
		
		finished.append (FDC_ID)
		
		
	except Exception as E:
		problem = {
			"exception": parse_exception.now (E).split ("\n"),
			"FDC_ID": FDC_ID
		}		
		print ("problem:", problem)
		problems.append (problem)
		
		problem_IDs.append (FDC_ID)


proceeds = simultaneously_v2 (
	items = ID_Scroll,
	capacity = 10,
	move = move
)


rich.print_json (data = {
	"problems": problems,
	"problem_IDs": problem_IDs,
	"finished": finished
})

print ("ID_Scroll:", ID_Scroll)
print ("ID_Scroll size:", len (ID_Scroll))
print ("Skipped size:", len (Skipped))
