

''''
	from goodest.shows_v2.recipe_with_goals._ops.build import build_recipe_with_goals
	from goodest.adventures.monetary.DB.goodest_tract.goals.retrieve_one import retrieve_one_goal
	recipe_packet = build_recipe_with_goals ({
		"IDs_with_amounts": [
			{
				"FDC_ID": "2677998",
				"packages": 1
			}
		],
		"goals": retrieve_one_goal ({
			"emblem": "10"
		})
	})
"'''


#|
#
from .procedures.add_goals import add_goals
#
#
from goodest.shows_v2.recipe._ops.retrieve import retrieve_recipe
#
#|

def build_recipe_with_goals (packet):
	IDs_with_amounts = packet ["IDs_with_amounts"]
	goals = packet ["goals"]
	
	''''
		TODO:
			Build the recipe packet.
	"'''
	recipe_packet = retrieve_recipe ({
		"location": "mongo",
		"IDs_with_amounts": IDs_with_amounts
	})
	recipe = recipe_packet ["recipe"]
	essential_nutrients_grove = recipe ["essential nutrients"] ["grove"]
	
	
	''' 
		TODO:
			This adds the goals to the essential_nutrients grove 
			of the recipe.
	'''
	add_goals ({
		"recipe": recipe,
		"essential_nutrients_grove": essential_nutrients_grove,
		"goals": goals,
		"records": 1
	})
	
	return recipe