

def Choline ():
	return {
		"labels": [
			"Choline"
		],
		
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "11/20"
						}
					}
				}
			},
		},
		
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}