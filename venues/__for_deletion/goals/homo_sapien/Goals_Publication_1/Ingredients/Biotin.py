


def Biotin ():
	return {
		"labels": [
			"Biotin"
		],
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "3/100000"
						}
					}
				}
			},
		},	
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}

