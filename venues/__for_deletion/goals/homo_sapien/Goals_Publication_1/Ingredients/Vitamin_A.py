


''''


"goal": {
	"mass + mass equivalents": {
		"per Earth day": {
			"grams": {
				"fraction string": "9/10000",
				
				#
				#	This is calculated
				#
				#
				"decimal string": "9.0000e-4"
			},
			
			#
			#	This is calculated
			#
			#
			"portion": {
				"fraction string": "4500/2309920787",
				"percent string": "0.00019481187516583008"
			}
		}
	}
}

"'''

''''
	1 gram = 1e6 mcg

	Vitamin A:
		Fully Grown:
			UL: 
				amount: 3,000 micro grams 
				
				notes: 
					Excess vitamin A is stored in the liver.
			
		Pregnant:
			UL:
				amount: 3,000 micro grams
			
			
		Origins:
			https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/
				"Tolerable upper intake levels for preformed vitamin A"
"'''
