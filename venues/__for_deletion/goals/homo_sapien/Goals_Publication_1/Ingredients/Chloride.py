


def Chloride ():
	return {
		"labels": [
			"Chloride"
		],
		
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "23/10"
						}
					}
				}
			},
		},
		
		
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}