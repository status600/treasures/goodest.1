

def Saturated_Fat ():
	return {
		"labels": [
			"Saturated Fat"
		],
		
		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "20"
						}
					}
				}
			},
		},
		
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}