


def Chromium ():
	return {
		"labels": [
			"Chromium"
		],

		"criteria": {
			"RDA": {
				"mass + mass equivalents": {
					"per Earth day": {
						"grams": {
							"fraction string": "7/200000"
						}
					}
				}
			},
		},
		
		"notes": [
		
		],
		"references": [
			"https://www.fda.gov/food/nutrition-facts-label/daily-value-nutrition-and-supplement-facts-labels"
		]
	}