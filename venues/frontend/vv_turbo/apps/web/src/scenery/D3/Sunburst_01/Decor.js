



import * as d3 from 'd3';

export default {
	name: 'SunburstChart',
	mounted() {
		this.drawSunburstChart();
	},
	
	data () {
		return {
			palette: {
				
			},
			
			ingredients: [{
				name: "Carbohydrates",
				grams: 14,
				ingredients: [{
					name: "Dietary Fiber",
					grams: 3,
				}]
			},{
				name: "Fats",
				grams: 12,
				ingredients: [{
					name: "Saturated Fat",
					grams: 4
				}]
			}]
		}
	},
	
	methods: {
		parse_qualities () {
			
			/*
			// Sample data
			const data_01 = {
				"name": "root",
				"children": [
					{ "name": "A", "size": 100 },
					{ "name": "B", "size": 300, "children": [
						{ "name": "B1", "size": 200 },
						{ "name": "B2", "size": 100 }
					]}
				]
			};
			*/
			
			const ingredients = this.ingredients;
			
			const root = {
				name: "root",
				children: []
			};

			function parseNode(node) {
				return {
					name: node.name,
					size: node.grams,
					children: node.ingredients ? node.ingredients.map(parseNode) : []
				};
			}

			root.children = ingredients.map (parseNode);
			return root;
		},
		
		add_labels ({
			svg,
			root
		}) {
			svg.append("g")
			.attr("pointer-events", "none")
			.attr("text-anchor", "middle")
			.attr("font-size", 10)
			.attr("font-family", "sans-serif")
			.selectAll("text")
			.data(root.descendants().filter(d => d.depth && (d.y0 + d.y1) / 2 * (d.x1 - d.x0) > 10))
			.join("text")
			.attr("transform", function(d) {
				const x = (d.x0 + d.x1) / 2 * 180 / Math.PI;
				const y = (d.y0 + d.y1) / 2;
				return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
			})
			.attr("dy", "0.35em")
			.text(d => d.data.name);
		},
		
		drawSunburstChart() {
			const width = 600;
			const radius = width / 2;
			
			const data = this.parse_qualities ();

			// Create an SVG container
			const svg = d3.select (this.$refs.latch)
				.append ('svg')
				.attr ('width', width)
				.attr ('height', width)
				.append ('g')
				.attr ('transform', `translate(${radius},${radius})`);

			const partition = d3.partition ()
				.size ([2 * Math.PI, radius]);

			// const root = partition(data);

			const root = d3.hierarchy (data)
				.sum(d => d.size)
				.sort((a, b) => b.value - a.value);

			partition (root);

			const arc = d3.arc ()
				.startAngle (d => d.x0)
				.endAngle (d => d.x1)
				.innerRadius (d => d.y0)
				.outerRadius (d => d.y1);

			svg
			.selectAll('path')
			.data (root.descendants())
			.enter ()
			.append ('path')
			.attr ('d', arc)
			.attr ('fill', d => {
				return "#44F"
				
				while (d.depth > 1) d = d.parent;
				return d3.schemeCategory10[ d.data.name.length % 10];
			})
			.attr ('stroke', '#fff')
			.attr ('stroke-width', '2px')
			.append ('title')
			.text (d => d.data.name);
				
			svg
			.selectAll ('text')
			.data (root.descendants())
			.enter()
			.append ('text')
			.attr ('transform', d => {
				/*const [x, y] = arc.centroid(d);
				const angle = (d.x0 + d.x1) / 2 - Math.PI / 2;
				const radius = (d.y0 + d.y1) / 2;
				return `translate(${x * radius},${y * radius}) rotate(${angle * 180 / Math.PI})`;
				*/
				
				const x = (d.x0 + d.x1) / 2 * 180 / Math.PI;
				const y = (d.y0 + d.y1) / 2;
				return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
			})
			.attr ('text-anchor', 'middle')
			.attr ('dy', '.35em')
			.text (d => d.depth > 0 ? d.data.name : '');
		}
	}
};
