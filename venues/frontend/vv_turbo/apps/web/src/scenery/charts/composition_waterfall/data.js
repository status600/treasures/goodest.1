
// https://d3js.org/d3-hierarchy/hierarchy
export const data = {
 "name": "",
 "children": [
  {
   "name": "CARBOHYDRATES",
   "children": [
    {
     "name": "DIETARY FIBER",
     "children": []
    },
    {
     "name": "SUGARS",
     "children": [
      {"name": "ADDED SUGARS", "value": 3534},

     ]
    }
   ]
  },
  {
   "name": "TOTAL FATS",
   "children": [
    {"name": "SATURATED FAT", "value": 17010},
    {
     "name": "TRANS FAT", "value": 0
    },
   ]
  },

 ]
}
