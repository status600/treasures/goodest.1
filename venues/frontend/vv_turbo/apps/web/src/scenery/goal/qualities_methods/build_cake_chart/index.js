


/*
	"criteria": {
		"RDA": {
			"mass + mass equivalents": {
				"per Earth day": {
					"grams": {
						"fraction string": "1/2500"
					}
				}
			}
		},
	}
*/


const cake_chart = {
	async build_cake_chart () {
		const goal = this.goal;
		const grove = goal.nature.recipe ["essential nutrients"].grove;
		
		let pastries = []
		for (let E = 0; E < grove.length; E++) {
			try {
				const name = grove [E].info.names [0]
				const amount = parseFloat (
					grove [E].goal.criteria.RDA ["mass + mass equivalents"] ["per Earth day"] ["grams"] ["sci note string"]
				)
				
				pastries.push ({
					label: name,
					data: amount
				})
			}
			catch (problem) {
				console.error ({ problem })
			}
		}
		
		this.$refs.mass_pie_chart.show_v2 ({
			wedges: pastries
		})
	}
}

export const { 
	build_cake_chart 
} = cake_chart;