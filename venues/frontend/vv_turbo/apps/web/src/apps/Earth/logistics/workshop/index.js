

/*
	priorities:
		component tests
		
			
	/@2/scenery
*/



var chassis = "/@2/"

export const workshop_routes = [
	{
		name: 'scenery',
		path: chassis + 'scenery',
		component: () => import ('@/regions/workshop/scenery/region.vue')
	}
]