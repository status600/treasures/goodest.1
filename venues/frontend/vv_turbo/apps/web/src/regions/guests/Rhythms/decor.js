




/*
	{ home, habitat }

*/

import s_panel from '@/scenery/panel/decor.vue'
import s_select from '@/scenery/select/decor.vue'
import s_line from '@/scenery/line/decor.vue'

import Topics from './topics/decor.vue'
	
export const decor = {
	components: { 
		Topics,
		
		s_panel,
		s_select,
		s_line
	},
	
	data () {
		return {}
	},
	
	methods: {
		on_change (option) {
			
		}
	}
}