

//\
//
import _get from 'lodash/get'
//
//
import { scan_goals } from '@/fleet/nutrition_meadow/goals/scan'
//
import goal_furniture from '@/scenery/goal/decor.vue'
import { open_goal } from '@/parcels/goal/open.js'
//
import s_curtain from '@/scenery/curtain/decor.vue'
//
///
import s_line from '@/scenery/line/decor.vue'
	
export const room = {
	inject: [ 'goals_store' ],
	
	components: {
		goal_furniture,
		s_curtain,
		s_line
	},
	
	data () {
		return {
			goals: [],
			goal: {},
			goal_picked: this.goals_store.warehouse ().goal_picked
		}		
	},
	created () {
		this.goals_store_monitor = this.goals_store.monitor (({ 
			inaugural, 
			field 
		}) => {
			this.goals = this.goals_store.warehouse ().goals;
			this.goal = this.goals_store.warehouse ().goal;
			this.goal_picked = this.goals_store.warehouse ().goal_picked;
			
			console.log ('goals_store_monitor', { 
				goals: this.goals, 
				goal: this.goal, 
				goal_picked: this.goal_picked
			})
		})
	},
	beforeUnmount () {
		this.goals_store_monitor.stop ()
	},
	
	methods: {
		
		_get,
		
		async open_goal () {
			await open_goal ({
				goal: this.goal
			})
		},
		
		open_goal_details ({ goal }) {
			open_goal ({
				goal
			})
		},
		
		async erase_goal () {
			await this.goals_store.moves.erase_goal ()
		},
		
		pick_goal ({ goal }) {
			this.pick ({ goal })
		},
		
		async pick ({ goal }) {
			console.log ("pick")
			
			await this.goals_store.moves.pick_goal ({ goal })
		},
		
		find_label () { 
			try {
				return this.goal.nature.label;
			}
			catch (exception) {
				
			}
			
			return ''
		}
	},
	
	async mounted () {
		await this.goals_store.moves.retrieve_goals ()
	}
	
}












///