

/*	
	import measured_ingredients_table from './fountains.vue'
	
	<measured_ingredients_table
		:grove="measured_ingredients"
	/>
*/


import cloneDeep from 'lodash/cloneDeep'

import { build_grove } from '@/grid/nature/essential_nutrients/grove/sort/cryo/grove-1'
import { sort_grove } from '@/grid/nature/essential_nutrients/grove/sort'
import { calc_linear_grove } from '@/grid/nature/essential_nutrients/grove/calc_linear_grove'
import { mass_plus_mass_eq } from '@/grid/nature/essential_nutrients/grove/ingredient/mass_plus_mass_eq'
import { round_quantity } from '@/grid/round_quantity'
import { fraction_to_float } from '@/grid/Fraction/to_float'

import s_select from '@/scenery/select/decor.vue'
import { has_field } from 'procedures/object/has_field'

import alert_problem from './decor/problem.vue'
import sci_note_component from './decor/sci_note.vue'


export const fountains = {
	props: [ "grove" ],
	
	components: { s_select, alert_problem, sci_note_component },
	
	computed: {
		linear_grove () {
			if (!Array.isArray (this.grove)) {
				return []
			}

			const grove = cloneDeep (this.grove);			
			sort_grove ({ grove })
			
			const linear_grove = calc_linear_grove ({ 
				grove
			})
			
			return linear_grove;
		}
	},
	
	methods: {
		name_1 (ingredient) {
			try {
				return ingredient ["name"];
			}
			catch (exception) {
				console.warn (
					'name not found:', 
					exception
				)				
			}
			
			return ''
		},
		
		parse_sci_note (sci_note_string) {
			try {
				const sci_note = sci_note_string.toUpperCase ()
				
				if (sci_note.includes ("E") !== true) {
					return {
						sci_note: [],
						sci_note_problem: "calc error"
					}
				}
				
				const split = sci_note.split ("E");
				
				return {
					sci_note: [ split [0], "E", split [1] ],
					sci_note_problem: ""
				}
			}
			catch (exception) {
				console.error (exception)
			}
			
			return {
				sci_note: [],
				sci_note_problem: "math error"
			}
		},
		
		listed_amount (ingredient) {
			/*
				relevant:
					from goodest.besties.food_USDA.nature_v2.measured_ingredients.measured_ingredient import build_measured_ingredient
					build_measured_ingredient ()
			*/
			
			/*
				possibilitity
			*/
			
			/*
			return {
				component: alert_problem,
				props: {
					sci_note_problem: "math error"
				}
			}*/
			
			
			
			try {				
				const measures = ingredient ["measures"];
				
				if (has_field (measures, "mass + mass equivalents")) {
					const grams = measures ["mass + mass equivalents"] ["per package"] ["grams"];
					const sci_note_string = grams ["scinote string"]
					const { sci_note, sci_note_problem } = this.parse_sci_note (sci_note_string);
					
					if (sci_note_problem.length >= 1) {
						return {
							component: alert_problem,
							props: {
								sci_note_problem: "math error"
							}
						}
					}
					return {
						component: sci_note_component,
						props: {
							sci_note,
							unit: "grams"
						}
					}
				}
				
				if (has_field (measures, "biological activity")) {
					const IU = measures ["biological activity"] ["per package"] ["IU"];
					const sci_note_string = IU ["scinote string"]
					const { sci_note, sci_note_problem } = this.parse_sci_note (sci_note_string);
					
					if (sci_note_problem.length >= 1) {
						return {
							component: alert_problem,
							props: {
								sci_note_problem: "math error"
							}
						}
					}
					return {
						component: sci_note_component,
						props: {
							sci_note,
							unit: "IU"
						}
					}
				}
				
				if (has_field (measures, "energy")) {
					const food_calories = measures ["energy"] ["per package"] ["food calories"];
					const sci_note_string = food_calories ["scinote string"]
					const { sci_note, sci_note_problem } = this.parse_sci_note (sci_note_string);
					
					if (sci_note_problem.length >= 1) {
						return {
							component: alert_problem,
							props: {
								sci_note_problem: "math error"
							}
						}
					}
					return {
						component: sci_note_component,
						props: {
							sci_note,
							unit: "food calories"
						}
					}
				}
			}
			catch (ex) {
				console.error (ex)
				
				/*
				console.warn (
					'mass + mass eq not found:', 
					ingredient ["essential"]["names"]
				)
				*/				
			}
			
			return {
				component: alert_problem,
				props: {
					sci_note_problem: "procedure error"
				}
			}
			
		},
		
		portion (ingredient) {
			try {				
				return [
					100 * fraction_to_float (
						ingredient ["measures"] ["mass + mass equivalents"] ["portion of grove"] ["fraction string"],
						false
					),
					"%"
				]
			}
			catch (ex) {
				/*
				console.warn (
					'mass + mass eq not found:', 
					ingredient ["essential"]["names"]
				)
				*/				
			}
			
			return ''
		},
		
		mass_plus_mass_eq (ingredient) {
			try {				
				return [
					fraction_to_float (
						ingredient ["measures"] ["mass + mass equivalents"] ["per package"] ["grams"] ["fraction string"],
						false
					),
					""
				]
			}
			catch (ex) {
				/*
				console.warn (
					'mass + mass eq not found:', 
					ingredient ["essential"]["names"]
				)
				*/				
			}
			
			return ''
		}
	}
}