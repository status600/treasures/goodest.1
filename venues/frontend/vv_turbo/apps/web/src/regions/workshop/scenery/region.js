

import change_indicator from '@/scenery/change_indicator/scenery.vue'
import Sunburst_01 from '@/scenery/D3/Sunburst_01/Decor.vue'

import JParticles from 'jparticles'

import s_select from '@/scenery/select/decor.vue'

	

export const region = {
	components: {
		s_select,
		
		change_indicator,
		Sunburst_01
	},
	
	data () {
		return {}
	},
	
	methods: {
		on_change (option) {
			
		}
	},
	
	mounted () {
		const canvas = this.$refs.canvas;

		
		new JParticles.Particle (canvas, {
			color: '#25bfff',
			lineShape: 'cube',
			range: 2000,
			proximity: 100,
			// Turn on parallax effect
			parallax: true,
		})
	}
}

