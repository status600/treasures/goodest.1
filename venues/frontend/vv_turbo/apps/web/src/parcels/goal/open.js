
/*
	import { open_goal } from '@/parcels/goal/open.js'
	open_goal ({
		goal
	})
*/

import { append_field } from '@/apps/fields/append'

export async function open_goal ({ goal = {} } = {}) {
	await append_field ({
		field_title: "goal",
		field: import ('@/parcels/goal/features.vue'),
		properties: {
			the_goal: goal
		}
	})
}