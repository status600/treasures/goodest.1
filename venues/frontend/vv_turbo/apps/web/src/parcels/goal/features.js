


import { goals_store } from '@/warehouses/goals'
import goal_furniture from '@/scenery/goal/decor.vue'

export const features = {
	inject: ['properties'],
	
	components: {
		goal_furniture
	},
	
	data () {
		return {
			goal: {}
		}
	},
	
	created () {
		const properties = this.properties;
		console.log ("goal properties", { properties });
		
		this.goals_store_monitor = goals_store.monitor (({ 
			inaugural, 
			field 
		}) => {
			this.goal = goals_store.warehouse ().goal;
			this.goal_picked = goals_store.warehouse ().goal_picked;
		})
	},
	
	beforeUnmount () {
		this.goals_store_monitor.stop ()
	}
}