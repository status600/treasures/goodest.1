






const olive_salad_button_v1 = {
	border: 'none',
	//background: 'linear-gradient(22deg, #ffffff14, #00000063)',
	//background: 'linear-gradient(22deg, rgba(255, 255, 255, 0.08), rgba(62, 62, 62, 0.72))',
	background: 'linear-gradient(22deg, rgb(68, 68, 68), rgb(53, 53, 53))',
	boxShadow: 'rgba(43, 4, 4, 0.89) 0px 2px 1px 0px, rgba(255, 255, 255, 0.26) 0px 0px 1px 0px inset',
	color: 'white'
}
export const olive_salad_button = {
	border: 'none',
	//background: 'linear-gradient(22deg, #ffffff14, #00000063)',
	//background: 'linear-gradient(22deg, rgba(255, 255, 255, 0.08), rgba(62, 62, 62, 0.72))',
	background: 'linear-gradient(-22deg, rgba(51, 153, 255, 0.89), rgba(155, 145, 206, 0.78))',
	boxShadow: 'rgba(43, 4, 4, 0.89) 0px 2px 1px 0px, rgba(255, 255, 255, 0.26) 0px 0px 1px 0px inset',
	color: 'white'
}
export const olive_salad_hw_button = {
	border: '4px solid rgba(34, 34, 34, 0)',
	background: 'linear-gradient(-10deg, rgba(202, 193, 130, 0), rgba(255, 251, 251, 0.23))',
	color: 'white'
}

/*
	border: 2px solid rgba(206, 200, 98, 0.84);
  background: linear-gradient(-10deg, rgba(230, 210, 18, 0.54), #e1c03f0f);
  color: black;
*/
export const cashew_salad_button_v1 = {
	border: 'none',
	background: 'linear-gradient(22deg, rgba(191, 168, 100, 0.03), rgba(172, 144, 63, 0.21))',
	boxShadow: 'rgba(185, 181, 164, 0.89) 0px 2px 1px 0px, rgba(164, 164, 164, 0.31) 0px 0px 1px 0px inset',
	color: 'black'
}
export const cashew_salad_button = {
	border: 'none',
	background: 'linear-gradient(22deg, rgba(153, 221, 255, 0.59), rgba(170, 204, 255, 0.35))',
	boxShadow: 'rgba(185, 181, 164, 0.89) 0px 2px 1px 0px, rgba(164, 164, 164, 0.31) 0px 0px 1px 0px inset',
	color: 'black'
}
export const cashew_salad_hw_button = {
	border: '4px solid rgba(34, 34, 34, 0)',
	background: 'linear-gradient(-10deg, rgba(83, 78, 29, 0), #4d1c1c59)',
	color: 'black'
}