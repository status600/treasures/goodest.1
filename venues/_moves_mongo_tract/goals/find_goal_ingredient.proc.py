




'''
	cd /goodest/venues/clinic/mongo_goals
'''

#----

import pathlib
from os.path import dirname, join, normpath
import sys
import os

import rich

this_directory = pathlib.Path (__file__).parent.resolve ()	
def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'../../stages',
	'../../stages_pip'
])

from goodest.adventures.monetary.DB.goodest_tract.goals.find_ingredient import find_goal_ingredient
goal_ingredient = find_goal_ingredient ({
	"region": "2",
	
	#
	#	The ingredient label (e.g. Biotin)
	#
	#
	"label": "sugar"
})

print ("goal_ingredient:", goal_ingredient)