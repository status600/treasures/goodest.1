




def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])



import goodest.adventures.monetary.DB.goodest_tract._land.insert_document as _land_insert_document
_land_insert_document.smoothly ({
	"collection": "cautionary_ingredients",
	"document": {
		'names': [ 'added sugars', 'sugars, added' ],
		'includes': [],
		'region': 8
	}
})