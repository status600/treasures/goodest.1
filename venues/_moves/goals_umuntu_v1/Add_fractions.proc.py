

'''
	python3 /habitat/venues/_controls/goals_umuntu/Add_fractions.py
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages'
])


import rich
from fractions import Fraction
from goodest.adventures.monetary.DB.goodest_tract.goals.insert import insert_goals_document
import goodest.goals.umuntu.FDA as FDA_goals_for_umuntu

goal = FDA_goals_for_umuntu.retrieve ()
insert_goals_document ({
	"document": goal
})

mass_sum = 0

rich.print_json (data = {
	"goal": goal
})
exit ()



ingredients = goal ["ingredients"]

exceptions = []

for ingredient in ingredients:
	try:
		mass_sum += Fraction (
			ingredient ["goal"] ["mass + mass equivalents"] ["per Earth day"] ["grams"] ["fraction string"]
		)
	except Exception as E:
		exceptions.append (ingredient)
		

goal ["statistics"] = {
	"sum": {
		"mass + mass equivalents": {
			"per Earth day": {
				"grams": {
					"fraction string": str (mass_sum),
					"decimal string": str (float (mass_sum))
				}
			}
		}
	}
}

for ingredient in ingredients:
	try:
		amount = ingredient ["goal"] ["mass + mass equivalents"] ["per Earth day"] ["grams"] ["fraction string"];
		portion = str (Fraction (amount) / Fraction (mass_sum))
	
		percent = str (float (Fraction (portion) * 100))
	
		ingredient ["goal"] ["mass + mass equivalents"] ["per Earth day"] ["portion"] = {
			"fraction string": portion,
			"percent string": percent
		}

	except Exception as E:
		exceptions.append (ingredient)


print ('exceptions:', exceptions)	
print ("mass_sum:", mass_sum)


rich.print_json (data = {
	"goal": goal
})

insert_goals_document ({
	"document": goal
})