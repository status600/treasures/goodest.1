

'''
	cd /habitat/venues/_controls/goals_umuntu
	python3 /habitat/venues/_controls/goals_umuntu/Add_fractions.py
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages'
])



import rich
from fractions import Fraction
from goodest.adventures.monetary.DB.goodest_tract.goals.insert import insert_goals_document
import goodest.goals.umuntu.FDA as FDA_goals_for_umuntu

goal = FDA_goals_for_umuntu.retrieve ()

insert_goals_document ({
	"document": goal
})

ingredients = goal ["ingredients"]





exceptions = []

mass_sum = 0
macros = 0
for ingredient in ingredients:
	try:
		label = ingredient ["labels"] [0]
		mass_plus_eq = Fraction (
			ingredient ["goal"] ["mass + mass equivalents"] ["per Earth day"] ["grams"] ["fraction string"]
		)
		
		if (label in [ "Protein", "Fats", "Saturated Fat", "Dietary Fiber", "carbohydrates" ]):
			macros += mass_plus_eq
		
		mass_sum += mass_plus_eq
		
	except Exception as E:
		exceptions.append (ingredient)
		

print ("macros:", float (macros / mass_sum) * 100)
print ("micros:", (100 - (float (macros / mass_sum) * 100)))

