

'''
	python3 /habitat/venues/_moves/goals_homo_sapien/make_and_insert.proc.py
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])


#\
#
from pprint import pprint
from fractions import Fraction
import json
#
#
import rich
#
#
from goodest.adventures.monetary.DB.goodest_tract.goals.insert import insert_goals_document
from goodest.shows_v2.goals.homo_sapien.Goals_Publication_1 import build_Goals_Publication_1
#
#/

goals = build_Goals_Publication_1 ()
#rich.print_json (data = goals)

def add (path, data):
	import pathlib
	from os.path import dirname, join, normpath
	this_directory = pathlib.Path (__file__).parent.resolve ()
	example_path = normpath (join (this_directory, path))
	FP = open (example_path, "w")
	FP.write (data)
	FP.close ()

add (
	"goals.JSON", 
	json.dumps (goals, indent = 4)
)

# exit ()

insert_goals_document ({
	"document": goals
})




###