



'''
	python3 /habitat/venues/_controls/goals_umuntu/Add_fractions.py
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/venues/stages'
])

#/
#
from goodest.adventures.monetary.DB.goodest_tract.goals.insert import insert_goals_document
from goodest.goals.umuntu.FDA_Publication_1 import build_FDA_Publication_1
#
from goodest.shows_v2.treasure.nature.land.grove._ops.seek_name_or_accepts import seek_name_or_accepts
from goodest.shows_v2.recipe._ops.retrieve import retrieve_recipe
from goodest.shows_v2.recipe_with_goals._ops.formulate import formulate_recipe_with_goals
from goodest.shows_v2.recipe._ops.formulate import formulate_recipe
#
#
#	foods
#
import goodest.besties.food_USDA.deliveries.one.assertions.foundational as assertions_foundational
import goodest.besties.food_USDA.examples as USDA_examples	
import goodest.besties.food_USDA.nature_v2 as food_USDA_nature_v2
#
#
#	supps
#
import goodest.besties.supp_NIH.nature_v2 as supp_NIH_nature_v2
import goodest.besties.supp_NIH.examples as NIH_examples
import goodest.mixes.insure.equality as equality
#
#	
import rich
#
#
from pprint import pprint
from fractions import Fraction
from copy import deepcopy
import json
#
#\


def find_grams (measures):
	return Fraction (
		measures ["mass + mass equivalents"] ["per recipe"] ["grams"] ["fraction string"]
	)
	
def add (path, data):
	import pathlib
	from os.path import dirname, join, normpath
	this_directory = pathlib.Path (__file__).parent.resolve ()
	example_path = normpath (join (this_directory, path))
	FP = open (example_path, "w")
	FP.write (data)
	FP.close ()
	
def retrieve_supp (supp_path):
	return supp_NIH_nature_v2.create (
		NIH_examples.retrieve (supp_path) 
	)

def retrieve_food (food_path):
	return food_USDA_nature_v2.create (
		USDA_examples.retrieve (food_path)
	)



recipe_packet = formulate_recipe_with_goals ({
	"recipe": formulate_recipe ({
		"natures_with_amounts": []	
	}),
	"goal_region": "3"
})	

recipe = recipe_packet ["recipe"]

add (
	"recipe.JSON", 
	json.dumps (recipe, indent = 4)
)



#goals = build_FDA_Publication_1 ();
#rich.print_json (data = goals)











###